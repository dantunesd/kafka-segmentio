package main

import (
	"context"
	"encoding/json"
	"log"
	"os"

	"github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/protocol"
)

func main() {
	kw := kafka.NewWriter(kafka.WriterConfig{
		Brokers: []string{"localhost:9092"},
		Topic:   "payments.webhook.events",
		Logger:  log.New(os.Stdout, "producer", 1),
	})

	transaction := GetTransaction()
	transactionEncoded, _ := json.Marshal(transaction)

	err := kw.WriteMessages(context.Background(),
		kafka.Message{
			Key:   []byte(transaction.Acquirer.TransactionID),
			Value: transactionEncoded,
			Headers: []protocol.Header{
				{
					Key:   "correlationId",
					Value: []byte("b5d5a2c5-c78f-4a9d-9b84-8774e7342220"),
				},
				{
					Key:   "event",
					Value: []byte("TRANSACTION_COMPLETED"),
				},
			},
		},
	)

	if err != nil {
		log.Fatal("failed to write messages:", err)
	}

	if err := kw.Close(); err != nil {
		log.Fatal("failed to close writer:", err)
	}
}
