package main

type EventName string

type Entrypoint string

const (
	Completed EventName = "TRANSACTION_COMPLETED"
	Failed    EventName = "TRANSACTION_FAILED"
	Cancelled EventName = "TRANSACTION_CANCELLED"
)

const (
	Offline Entrypoint = "OFFLINE"
	Online  Entrypoint = "ONLINE"
)

type TransactionEvent struct {
	Event       Event       `json:"event"`
	Merchant    Merchant    `json:"merchant"`
	Acquirer    Acquirer    `json:"acquirer"`
	Transaction Transaction `json:"transaction"`
}

type Event struct {
	Name string `json:"name"`
	Date string `json:"date"`
}

type Merchant struct {
	ID string `json:"id"`
}

type Acquirer struct {
	Name          string `json:"name"`
	TransactionID string `json:"transaction_id"`
	Nsu           string `json:"nsu"`
}

type Transaction struct {
	ID             string `json:"id"`
	Amount         int    `json:"amount"`
	Installments   int    `json:"installments"`
	Type           string `json:"type"`
	Brand          string `json:"brand"`
	Entrypoint     string `json:"entrypoint"`
	Currency       string `json:"currency"`
	SoftDescriptor string `json:"soft_descriptor"`
}

func GetTransaction() *TransactionEvent {
	return &TransactionEvent{
		Event: Event{
			Name: "TRANSACTION_COMPLETED",
			Date: "2021-10-28T19:47:06Z",
		},
		Merchant: Merchant{
			ID: "10e160b4-cf0f-4538-8e20-49fe29037e15",
		},
		Acquirer: Acquirer{
			Name:          "ZOOP",
			TransactionID: "4c0f5a604fa14e2ebc6c79d02c1a04c8",
			Nsu:           "132517191",
		},
		Transaction: Transaction{
			ID:             "ffc279e4-ca04-43fb-ab52-4d1648149074",
			Amount:         11,
			Installments:   1,
			Type:           "CREDIT",
			Brand:          "MASTERCARD",
			Entrypoint:     "OFFLINE",
			Currency:       "BRL",
			SoftDescriptor: "ALIMENTOS DA VILA",
		},
	}
}
