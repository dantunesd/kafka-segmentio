package main

import (
	"context"
	"fmt"
	"log"

	"github.com/segmentio/kafka-go"
)

func main() {

	kr := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{"localhost:9092"},
		GroupID: "payments",
		Topic:   "my-topic",
	})

	for {
		m, err := kr.FetchMessage(context.Background())
		if err != nil {
			log.Println("falha ao ler mensagem:", err)
			break
		}

		fmt.Printf("message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
		kr.CommitMessages(context.Background(), m)
	}

	if err := kr.Close(); err != nil {
		log.Println("failed to close reader:", err)
	}
}
