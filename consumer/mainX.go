package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"

	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
)

type LoggerAdapter struct {
	l *zap.Logger
}

func (l LoggerAdapter) Printf(msg string, args ...interface{}) {
	l.l.Info(msg)
}

func MainX() {
	logger, _ := zap.NewProduction()
	loggerAdapter := LoggerAdapter{
		l: logger,
	}

	// make a new reader that consumes from topic-A
	kr := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{"localhost:9092"},
		GroupID: "cozinha",
		Topic:   "my-topic",
		Logger:  loggerAdapter,
	})

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt)

	defer func() {
		signal.Stop(signalChan)
		cancel()
	}()

	go func() {
		sig := <-signalChan
		if sig != nil {
			log.Println("Sinal recebido")
			cancel()
		}
	}()

	for {

		m, err := kr.FetchMessage(ctx)
		if err != nil {
			log.Println("falha ao ler mensagem:", err)
			break
		}

		go func() {
			log.Println("starting go func:")

			fmt.Printf("message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))

			kr.CommitMessages(context.Background(), m)

			time.Sleep(10 * time.Second)

			log.Println("finishing go func:")

		}()
	}

	if err := kr.Close(); err != nil {
		log.Println("failed to close reader:", err)
	}
}
